import Footer from "../components/Footer";
import Landing from "../components/Landing";
import NavBar from "../components/NavBar";

const LandingPage = () => {
  return (
    <div>
      <NavBar />
      <Landing />
      <Footer />
    </div>
  );
};

export default LandingPage;
