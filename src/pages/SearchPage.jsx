import Footer from "../components/Footer";
import PcSearch from "../components/PcSearch";
import NavBar from "../components/NavBar";

const SearchPage = () => {
  return (
    <div>
      <NavBar />
      <PcSearch/>
      <Footer />
    </div>
  );
};

export default SearchPage;
