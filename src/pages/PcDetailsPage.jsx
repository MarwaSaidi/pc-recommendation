import React from "react";
import Footer from "../components/Footer";
import PcDetails from "../components/PcDetails";
import NavBar from "../components/NavBar";
const PcDetailsPage = () => {
  return (
    <div>
      <NavBar />
      <PcDetails/>
      <Footer />
    </div>
  );
};

export default PcDetailsPage;
