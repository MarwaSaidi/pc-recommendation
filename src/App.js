import { Route, Routes } from "react-router-dom";
import LandingPage from "./pages/LandingPage";
import NotFound from "./components/NotFound";
import PcSearch from "./components/PcSearch";
import PcList from "./components/PcList";
import PcDetails from "./components/PcDetails";

function App() {
  return (
    <Routes>
      <Route path="/" element={<LandingPage />} />
      <Route path="/search" element={<PcSearch />}>
        <Route path="pcList" element={<PcList />} />
        <Route path="details" element={<PcDetails />} />
      </Route>
      <Route path="*" element={<NotFound />} />
    </Routes>

  );
}

export default App;
