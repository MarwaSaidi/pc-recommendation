import styled from "styled-components";

export const Navbar = styled.div`
  height: 70px;
  background-color: #3c413f;
  display: flex;
  justify-content: center;
`;
export const NavContainer = styled.div`
  width: 100%;
  max-width: 1024px;
  color: white;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
export const Logo = styled.span`
  font-family: "Poppins";
  font-style: normal;
  font-weight: 600;
  font-size: 29px;
  line-height: 42px;
`;
export const NavItem = styled.div``;
export const Button = styled.button`
  margin-left: 20px;
  border: none;
  padding: 5px 10px;
  cursor: pointer;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 600;
  font-size: 12px;
  line-height: 18px;
  background: linear-gradient(97.57deg, #ffd369 -9.53%, #ffebbb 103.18%);
  border-radius: 10px;
`;