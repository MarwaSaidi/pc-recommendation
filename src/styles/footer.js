import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  justify-content: center;
  background: #282828;
`;
export const Wrapper = styled.div`
  width: 100%;
  max-width: 1024px;
  color: white;
  color: white;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
export const Left = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;

`;

export const Logo = styled.h1`
width: 400px;
  font-family: "Poppins";
  font-size: 36px;
  line-height: 42px;
  color: #ffffff;
  @media (max-width: 900px) {
    width: 200px;
  }
`;

export const Desc = styled.p`

  margin: 20px 0px;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 24px;
  color: #b9b8bb;
`;

export const SocialContainer = styled.div`
  display: flex;
`;

export const SocialIcon = styled.div`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  color: white;
  background-color: #28303F;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 20px;
`;

export const Center = styled.div`
  flex: 1;
  padding: 20px;
  
`;

export const Title = styled.h3`
  margin-bottom: 30px;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 700;
  font-size: 18px;
  line-height: 24px;
`;

export const List = styled.ul`
  margin: 0;
  padding: 0;
  list-style: none;
`;

export const ListItem = styled.li`
  width: 50%;
  margin-bottom: 10px;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 22px;
  color: #b9b8bb;
`;

export const Right = styled.div`
  flex: 1;
  padding: 20px;

`;

export const Item = styled.div`
  margin-bottom: 20px;
  display: flex;
  align-items: center;
  font-family: "Urbanist";
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 22px;
  color: #b9b8bb;

`;
export const Div=styled.div`
padding-top:20px;
font-family: 'DM Sans';
font-style: normal;
font-weight: 400;
font-size: 14px;
line-height: 22px;
color: #565660;
`;
export const InputContainer=styled.div`
display: flex;
border: 1px solid #A9A9C2;
border-radius: 43px;
@media (max-width: 900px) {
    width: 105px;
  }
`;
export const Input=styled.input`
width:200px;
background: #282828;
margin-left:15px;
border:none;
outline: none;
border-radius:20px 0px 0px 20px;
::placeholder {
    font-family: 'DM Sans';
    font-style: normal;
    font-weight: 400;
    font-size: 14px;
    line-height: 22px;
    color: #B9B8BB;
  }
  @media (max-width: 900px) {
    width: 50px;
  }

`
export const SendIcon = styled.button`
  width: 40px;
  height: 40px;
  color: white;
  background-color: #${(props) => props.color};
  display: flex;
  align-items: center;
  justify-content: center;
  border:none;
  border-radius:0px 20px 20px 0px;
  cursor:pointer;
`;
