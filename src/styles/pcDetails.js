import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  max-width: 100%;
  justify-content: space-between;

  margin-left: 30px;
  @media (max-width: 1000px) {
    height: 100%;
  }
`;
export const Info = styled.div`
padding:20px;
flex-direction: column;
margin-right: 15px;
margin-bottom:15px;
width: 640px;
height:1200px:
display: flex;
position: relative;
background: #3c413f;
border-radius: 30px;

@media (max-width: 1000px) {
  width: 85%;
}
`;
export const Price = styled.div`
  float: right;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 600;
  font-size: 21px;
  line-height: 32px;
  text-align: right;
  letter-spacing: 0.1px;
  color: #fffcf3;
`;
export const ImgContainer = styled.div`
  width: 600px;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  @media (max-width: 900px) {
    width: 60%;
  }
`;

export const Imag = styled.img`
  width: 180px;
  height: 180px;
`;
export const Name = styled.div`
  width: 100%;
  max-width: 700px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-top: 45px;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 600;
  font-size: 26px;
  line-height: 36px;
  letter-spacing: 0.1px;
  color: #f1f1f5;
`;
export const IconContainer = styled.div`
  display: flex;
`;
export const IconBookmark = styled.div`
  text-align: center;
  height: 36px;
  width: 36px;
  border: 1px solid #ffd369;
  color: #ffd369;
  border-radius: 10px;
  cursor: pointer;
  margin-left: 20px;
  margin-right: 20px;
`;
export const IconShare = styled.div`
  text-align: center;
  height: 36px;
  width: 36px;
  border: 1px solid #e2e2ea;
  color: #e2e2ea;
  border-radius: 10px;
  cursor: pointer;
`;
export const DetailsInfo = styled.div`
  width: 100%;
  max-width: 700px;
  color: white;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
export const DetailsItem = styled.div`
  display: flex;
`;
export const P1 = styled.p`
  margin-right: 10px;
  font-family: "Roboto";
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 16px;
  letter-spacing: 0.1px;

  color: #fff2d2;
`;
export const P2 = styled.p`
  font-family: "Roboto";
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 16px;
  letter-spacing: 0.1px;
  color: #ffffff;
`;
export const Properties = styled.div`
  position: relative;
  height: 80px;
  margin-top: 25px;
  background-color: #3c413f;
  border: none;
  display: flex;
  align-items: center;
  justify-content: space-around;
  border-radius: 10px;
  width: 100%;
  max-width: 1324px;

  border: 1px solid #fafafb;
`;
export const PropertiesItem = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding-right: 40px;
  gap: 10px;
`;

export const PropertiesName = styled.div`
  font-family: "Roboto";
  font-style: normal;
  font-weight: 400;
  font-size: 12px;
  line-height: 22px;
  /* identical to box height, or 183% */
  color: #ececff;
`;
export const PropertiesValue = styled.div`
  font-family: "Roboto";
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 16px;
  letter-spacing: 0.1px;

  color: #fff2d2;
`;
export const OverView = styled.div`
  font-family: "Poppins";
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 24px;
  letter-spacing: 0.1px;
  color: #f1f1f5;
  margin-top: 25px;
`;

export const OverViewContent = styled.div`
  max-width: 600px;
  font-family: "Roboto";
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 28px;
  letter-spacing: 0.1px;
  color: #fffcf3;
  margin-top: 25px;
`;
export const Fiche = styled.div`
  font-family: "Poppins";
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 24px;
  letter-spacing: 0.1px;
  color: #f1f1f5;
  margin-top: 25px;
`;
export const FicheContent = styled.div`
  margin-top: 15px;
  font-family: "Roboto";
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 16px;
  letter-spacing: 0.1px;
  color: #fffcf3;
  display: flex;
  align-items: center;
`;
export const Icon = styled.div`
  margin-right: 10px;
  align-items: center;
  color: #3dd598;
`;
export const Line = styled.div`
  height: 1px;
  width: 100%;
  background: #fffcf3;
  margin-top: 25px;
  border-radius: 10px;
`;
export const Confirm = styled.div`
  width: 100%;
  max-width: 700px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-top: 25px;
`;
export const Text = styled.div`
  font-family: "Poppins";
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 24px;
  /* identical to box height */

  letter-spacing: 0.1px;

  color: #f1f1f5;
`;
export const Button = styled.button`
  background: #ffd369;
  border: none;
  border-radius: 10px;
  padding: 15px 35px 15px 35px;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 600;
  font-size: 12px;
  text-align: center;
  color: #0d120e;
  cursor: pointer;
`;
export const PcContainer = styled.div`
  flex: 1;
  width: 309px;
  position: relative;
  display: flex;
  flex-direction: column;
  @media (max-width: 1200px) {
    width: 85%;
  }
`;
export const PcWrapper = styled.div`
  height: 98px;
  width: 279px;
  border-radius: 10px;
  background: #3c413f;
  margin: 15px;
  display: flex;
  padding: 20px;
  @media (max-width: 900px) {
    width: 80%;
  }
`;

export const PcImg = styled.img`
  height: 48px;
  width: 48px;
  border-radius: 5px;
  margin-right: 10px;
`;
export const PcInfo = styled.div`
  display: flex;
  flex-direction: column;
  margin-right: 30px;
`;
export const PcInfoTitle = styled.div`
  margin-bottom: 10px;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 21px;
  letter-spacing: 0.1px;
  color: #f6f6f6;
`;
export const PcInfoSite = styled.div`
  margin-bottom: 20px;
  font-family: "Roboto";
  font-style: normal;
  font-weight: 400;
  font-size: 12px;
  line-height: 14px;
  color: #f6f6f6;
`;
export const ButtonContainer = styled.div`
  display: flex;
`;
export const PcViewButton = styled.button`
  height: 28px;
  width: 62px;
  background: #f6f6f6;
  border-radius: 10px;
  padding: 3px;
  border: none;
  cursor: pointer;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 600;
  font-size: 12px;
  line-height: 18px;
  text-align: center;

  color: #000000;
`;
export const PcfavButton = styled.div`
margin-left:20px;
  color:white;
  cursor:pointer;
`;
export const PcPrice = styled.div`
  font-family: "Poppins";
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 21px;
  text-align: right;
  letter-spacing: 0.1px;
  color: #f6f6f6;
`;
export const ButtonSeemore=styled.div`
max-width: 339px;
padding:10px;
margin-left:15px;
background: #F1F1F5;
border-radius: 10px;
font-family: 'Poppins';
font-style: normal;
font-weight: 600;
font-size: 12px;
line-height: 18px;
/* identical to box height */
text-align: center;
color: #696974;
`;