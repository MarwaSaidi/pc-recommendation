import styled from "styled-components";

export const LaptopList = styled.div`
  background-color: ;
  display: flex;
  width: 1000px;
  flex-direction: column;
  margin-left: 30px;
  @media (max-width: 900px) {
    width: 60%;
  }
`;
export const TitleContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
`;
export const Title1 = styled.div`
  width: 100%;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 600;
  font-size: 24px;
  line-height: 36px;
  align-items: center;
  letter-spacing: 0.1px;
  color: #f6f6f6;
  order: 1;
`;
export const Title2 = styled.div`
  position: relative;
  left: 310px;
  width: 100%;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  font-family: "Roboto";
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
  line-height: 21px;
  text-align: right;
  letter-spacing: 0.1px;
  color: #b5b5be;
  order: 2;
  @media (max-width: 900px) {
    left: 8px;
  }
`;
export const Icon = styled.div`
  display: flex;
  color: #92929d;
  font-family: "Roboto";
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 21px;
  text-align: right;
  letter-spacing: 0.1px;
  margin-left: 20px;
`;
export const Container1 = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
`;

export const Info = styled.div``;

export const Container = styled.div`
  flex: 1;
  margin: 15px 15px 0px 0px;
  min-width: 262px;
  padding: 15px 10px 0px 15px;
  display: flex;
  position: relative;
  background: #3c413f;
  border-radius: 30px;
`;

export const Image = styled.img`
  border-radius: 10px;
  height: 90px;
  width: 90px;
  z-index: 2;
`;
export const Title = styled.div`
  position: relative;
  margin-top: 15px;
  display: flex;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 24px;
  letter-spacing: 0.1px;
  color: #f6f6f6;
`;
export const IconTitle = styled.div`
  position: absolute;
  color: #${(props) => props.color};
  left: 170px;
`;
export const DescContainer = styled.div`
  display: flex;
`;
export const Desc = styled.div`
  margin-top: 15px;
  font-family: "Roboto";
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 23px;
  letter-spacing: 0.1px;
  color: #fffcf3;
  height: 72px;
  width: 250px;
  overflow: hidden;
  text-overflow: ellipsis;
  display: block;
`;
export const DescEllipsis = styled.div`
  margin-top: 60px;
  color: #fffcf3;
`;
export const CategoriesContainer = styled.div`
  display: flex;
`;
export const Categories = styled.div`
  background: #f1f1f51a;
  padding: 12px;
  margin-top: 20px;
  margin-right: 10px;
  border-radius: 5px;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 500;
  font-size: 10px;
  line-height: 15px;
  text-align: center;
  color: rgba(255, 255, 255, 0.8);
`;

export const Website = styled.div`
  display: flex;
  margin-top: 20px;
`;
export const WebsiteTitle = styled.div`
  margin-right: 5px;
  font-family: "Roboto";
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 16px;
  letter-spacing: 0.1px;
  color: #fff2d2;
`;

export const Button = styled.button`
  width: 142px;
  padding: 10px;
  border: none;
  margin-top: 20px;
  margin-bottom: 25px;
  background: #ffd369;
  border-radius: 10px;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 600;
  font-size: 12px;
  line-height: 18px;
  text-align: center;
  color: #0d120e;
  cursor: pointer;
`;