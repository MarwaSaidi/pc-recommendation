import styled from "styled-components";
import { motion } from "framer-motion";

export const Container = styled.div`
  background-color: #1e1e1e;
  width: 100%;
  height: 1100px;
  overflow: hidden;

  @media (max-width: 900px) {
    height: 1150px;
  }
`;
export const Circle1 = styled.div`
  position: relative;
  border: 2px solid #ffffff;
  border-radius: 50%;
  border-color: rgba(255, 255, 255, 0.05);
  width: 600px;
  height: 600px;
  position: relative;
  margin-left: -350px;
  margin-top: -390px;
`;
export const Circle2 = styled.div`
  border: 2px solid #ffffff;
  border-radius: 50%;
  border-color: rgba(255, 255, 255, 0.05);
  width: 550px;
  height: 550px;
  margin: 45px 0px 0px 20px;
`;

export const Circle3 = styled.div`
  border: 2px solid #ffffff;
  border-radius: 50%;
  border-color: rgba(255, 255, 255, 0.05);
  width: 500px;
  height: 500px;
  margin: 45px 0px 0px 15px;
`;
export const Circle4 = styled.div`
  border: 40px solid #ffebbb;
  border-radius: 50%;
  width: 400px;
  height: 400px;
  margin: 15px 0px 0px 0px;
`;
export const Wrapper = styled.div`
  position: absolute;
  width: 70%;
  left: 240px;
  top: 160px;
  z-index: 2;

  justify-content: center;
  @media (max-width: 900px) {
    left: 0px;
  }
`;
export const Title = styled.div`
  width: 60%;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 600;
  font-size: 72px;
  line-height: 90px;
  letter-spacing: 0.1px;
  text-transform: capitalize;

  color: #f6f6f6;
`;

export const Text = styled.p`
  font-family: "Poppins";
  font-style: normal;
  font-weight: 400;
  font-size: 26px;
  line-height: 90px;
  letter-spacing: 0.1px;
  text-transform: capitalize;
  color: rgba(246, 246, 246, 0.7);
`;
export const Budget = styled.div`
  width: 508px;
  height: 166px;
  background: #3c413f;
  box-shadow: inset -1px -1px 0px rgba(241, 241, 245, 0.2);
  border-radius: 10px;
  flex: 1;
  align-items: center;
  justify-content: center;
  margin-top: -40px;
  @media (max-width: 900px) {
    width: 458px;
  }
`;
export const Icon = styled.div`
  color: #${(props) => props.color};
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 15px;
`;
export const TextBudget = styled.div`
  font-family: "Roboto";
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 26px;
  letter-spacing: 0.1px;
  color: #f6f6f6;
  display: flex;
  margin-left: 20px;
`;
export const SliderContainer = styled(motion.div)`
  overflow: hidden;
  cursor: grab;
  width: 95%;
  margin-top: -40px;
  display: flex;
`;
export const InnerSlider = styled(motion.div)`
  display: flex;
`;
export const Item = styled(motion.div)`
  position: relative;
  height: 10rem;
  min-width: 20rem;
  padding: 2px 15px;
`;
export const Img = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  border-radius: 20px;
  pointer-events: none;
`;
export const InfoImg = styled.div`
  position: absolute;
  height: 57px;
  width: 320px;
  left: 4.2%;
  right: 0%;
  top: 65%;
  background: rgba(0, 0, 0, 0.08);
  backdrop-filter: blur(10px);
  border-radius: 0px 0px 20px 20px;
`;
export const TitleImg = styled.p`
  font-family: "DM Sans";
  font-style: normal;
  font-weight: 700;
  font-size: 20px;
  line-height: 26px;
  text-align: center;
  color: #ffffff;
  margin-top: 10px;
`;

export const Button = styled.button`
  height: 70px;
  width: 269px;
  background: #ffd369;
  border-radius: 10px;
  border: none;
  cursor: pointer;
  margin-top: 40px;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 700;
  font-size: 18px;
  line-height: 27px;
  text-align: center;
  letter-spacing: 1.1px;
  color: #3c413f;
`;
export const OuterCircle = styled.div`
  border: 2px solid #ffffff;
  border-radius: 50%;
  border-color: rgba(255, 255, 255, 0.1);
  width: 660px;
  height: 660px;
  position: relative;
  margin-left: 250px;
  margin-top: 620px;
  @media (max-width: 900px) {
    margin-top: 620px;
    margin-left: 330px;
  }
`;

export const InnerCircle = styled.div`
  border: 2px solid #ffffff;
  border-radius: 50%;
  border-color: rgba(255, 255, 255, 0.05);
  width: 550px;
  height: 550px;
  margin: 50px 0px 0px 50px;
`;
export const InnerCircle1 = styled.div`
  border: 40px solid #ffebbb;
  border-radius: 50%;
  width: 400px;
  height: 400px;
  margin: 40px 0px 0px 40px;
`;
export const Div = styled.div`
  position: absolute;
  overflow: hidden;
  width: 650px;
  height: 1100px;
  top: 72px;
  right: 0px;

  @media (max-width: 900px) {
    height: 1150px;
  }
`;
export const ImgContainer = styled.div`
  position: absolute;
  width: 650px;
  height: 810px;
  right: 0px;
`;
export  const Image = styled.img`
  width: 100%;
  height: 100%;
  margin-top: -140px;
`;
export const Filter = styled.div`
  width: 80px;
  height: 80px;
  margin-left: 320px;
  background: linear-gradient(
    97.26deg,
    #6946f5 20.98%,
    #1861ef 58.39%,
    #29e9af 101.23%
  );
  opacity: 0.8;
  filter: blur(78.5px);
  transform: matrix(-1, 0, 0, 1, 0, 0);
`;
export const WrapperSlider = styled.div`
  display: flex;
  margin-left: -85px;
`;
export const Arrow = styled.div`
  position: relative;
  left: 85px;
  top: 20px;
  width: 30px;
  height: 30px;
  border-radius: 50%;
  color: black;
  background-color: white;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 20px;
  cursor: pointer;
  z-index: 2;
`;
export const ArrowForward = styled.div`
  position: relative;
  left: 95%;
  top: 20px;
  width: 30px;
  height: 30px;
  border-radius: 50%;
  color: black;
  background-color: white;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 20px;
  cursor: pointer;
  z-index: 2;
  @media (max-width: 900px) {
  }
`;