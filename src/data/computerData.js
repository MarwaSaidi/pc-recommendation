export const images = [
    {
      id: 1,
      img: "/assets/pc_gamer.jpg",
      title: "Gaming",

    },
    {
      id: 2,
      img: "/assets/laptop.jpg",
      title: "Laptop",

    },
    {
      id: 3,
      img: "/assets/laptop_pro.jpg",
      title: "Laptop Pro",

    },
    {
      id: 4,
      img: "/assets/bureau.jpg",
      title: "Bureau",

    },
]

export const data = [
  {
    id: 1,
    img: "/assets/pc_gamer.jpg",
    name: "ASUS ExpertBook L1",
    desc:"Écran 14 HD - Processeur: Intel Core i3-1115G4 (3.00 GHz up to 4,10 GHz Turbo max , 6 Mo de mémoire cache, Dual-Core) - Système d'exploitation: FreeDos - Mémoire RAM: 4 Go - Disque dur: 1 To HDD - Carte graphique: Intel HD Graphics avec WiFi, Bluetooth, 1x USB 2.0 Type-A, 2x USB 3.2 Gen 1 Type-A, 1x HDMI 1.4,1x RJ-45, 1 prise jack pour casque 3.5 mm - Couleur: Noir - Garantie: 1 an",
    categories:["Gaming","Bureau","Portable Pro"],
    site:"Scoop Informatique",
    stock:"En Stock",
    price:500,

  },
  {
    id: 2,
    img: "/assets/laptop.jpg",
    name: "ASUS ExpertBook L1",
    desc:"Écran 14 HD - Processeur: Intel Core i3-1115G4 (3.00 GHz up to 4,10 GHz Turbo max , 6 Mo de mémoire cache, Dual-Core) - Système d'exploitation: FreeDos - Mémoire RAM: 4 Go - Disque dur: 1 To HDD - Carte graphique: Intel HD Graphics avec WiFi, Bluetooth, 1x USB 2.0 Type-A, 2x USB 3.2 Gen 1 Type-A, 1x HDMI 1.4,1x RJ-45, 1 prise jack pour casque 3.5 mm - Couleur: Noir - Garantie: 1 an",
    categories:["Gaming","Bureau","Portable Pro"],
    site:"Mytek",
    stock:"rupture de stock",
    price:800,
  },
  {
    id: 3,
    img: "/assets/laptop_pro.jpg",
    name: "ASUS ExpertBook L1 ",
    desc:"Écran 14 HD - Processeur: Intel Core i3-1115G4 (3.00 GHz up to 4,10 GHz Turbo max , 6 Mo de mémoire cache, Dual-Core) - Système d'exploitation: FreeDos - Mémoire RAM: 4 Go - Disque dur: 1 To HDD - Carte graphique: Intel HD Graphics avec WiFi, Bluetooth, 1x USB 2.0 Type-A, 2x USB 3.2 Gen 1 Type-A, 1x HDMI 1.4,1x RJ-45, 1 prise jack pour casque 3.5 mm - Couleur: Noir - Garantie: 1 an",
    categories:["Gaming","Bureau","Laptop Pro","Laptop "],
    site:"Scoop Informatique",
    stock:"En Stock",
    price:2200,
  },
  {
    id: 4,
    img: "/assets/bureau.jpg",
    name: "ASUS ExpertBook L1",
    desc:"Écran 14 HD - Processeur: Intel Core i3-1115G4 (3.00 GHz up to 4,10 GHz Turbo max , 6 Mo de mémoire cache, Dual-Core) - Système d'exploitation: FreeDos - Mémoire RAM: 4 Go - Disque dur: 1 To HDD - Carte graphique: Intel HD Graphics avec WiFi, Bluetooth, 1x USB 2.0 Type-A, 2x USB 3.2 Gen 1 Type-A, 1x HDMI 1.4,1x RJ-45, 1 prise jack pour casque 3.5 mm - Couleur: Noir - Garantie: 1 an",
    categories:["Gaming","Bureau","Laptop Pro","Laptop "],
    site:"Mega PC",
    stock:"En Stock",
    price:1750,
  },
  {
    id: 5,
    img: "/assets/pc_gamer.jpg",
    name: "ASUS ExpertBook L1",
    desc:"Écran 14 HD - Processeur: Intel Core i3-1115G4 (3.00 GHz up to 4,10 GHz Turbo max , 6 Mo de mémoire cache, Dual-Core) - Système d'exploitation: FreeDos - Mémoire RAM: 4 Go - Disque dur: 1 To HDD - Carte graphique: Intel HD Graphics avec WiFi, Bluetooth, 1x USB 2.0 Type-A, 2x USB 3.2 Gen 1 Type-A, 1x HDMI 1.4,1x RJ-45, 1 prise jack pour casque 3.5 mm - Couleur: Noir - Garantie: 1 an",
    categories:["Gaming","Bureau","Portable Pro","Laptop Pro"],
    site:"Scoop Informatique",
    stock:"En Stock",
    price:3200,

  },
  {
    id: 6,
    img: "/assets/laptop_pro.jpg",
    name: "ASUS ExpertBook L1 ",
    desc:"Écran 14 HD - Processeur: Intel Core i3-1115G4 (3.00 GHz up to 4,10 GHz Turbo max , 6 Mo de mémoire cache, Dual-Core) - Système d'exploitation: FreeDos - Mémoire RAM: 4 Go - Disque dur: 1 To HDD - Carte graphique: Intel HD Graphics avec WiFi, Bluetooth, 1x USB 2.0 Type-A, 2x USB 3.2 Gen 1 Type-A, 1x HDMI 1.4,1x RJ-45, 1 prise jack pour casque 3.5 mm - Couleur: Noir - Garantie: 1 an",
    categories:["Gaming","Bureau","Portable Pro"],
    site:"Scoop Informatique",
    stock:"En Stock",
    price:1250,
  },
  {
    id: 7,
    img: "/assets/laptop.jpg",
    name: "ASUS ExpertBook L1",
    desc:"Écran 14 HD - Processeur: Intel Core i3-1115G4 (3.00 GHz up to 4,10 GHz Turbo max , 6 Mo de mémoire cache, Dual-Core) - Système d'exploitation: FreeDos - Mémoire RAM: 4 Go - Disque dur: 1 To HDD - Carte graphique: Intel HD Graphics avec WiFi, Bluetooth, 1x USB 2.0 Type-A, 2x USB 3.2 Gen 1 Type-A, 1x HDMI 1.4,1x RJ-45, 1 prise jack pour casque 3.5 mm - Couleur: Noir - Garantie: 1 an",
    categories:["Gaming","Bureau","Portable Pro"],
    site:"Mytek",
    stock:"rupture de stock",
    price:5200,
  },
  {
    id: 8,
    img: "/assets/laptop_pro.jpg",
    name: "ASUS ExpertBook L1 ",
    desc:"Écran 14 HD - Processeur: Intel Core i3-1115G4 (3.00 GHz up to 4,10 GHz Turbo max , 6 Mo de mémoire cache, Dual-Core) - Système d'exploitation: FreeDos - Mémoire RAM: 4 Go - Disque dur: 1 To HDD - Carte graphique: Intel HD Graphics avec WiFi, Bluetooth, 1x USB 2.0 Type-A, 2x USB 3.2 Gen 1 Type-A, 1x HDMI 1.4,1x RJ-45, 1 prise jack pour casque 3.5 mm - Couleur: Noir - Garantie: 1 an",
    categories:["Gaming","Bureau","Portable Pro","Laptop"],
    site:"Scoop Informatique",
    stock:"En Stock",
    price:1200,
  },
  {
    id: 9,
    img: "/assets/laptop.jpg",
    name: "ASUS ExpertBook L1",
    desc:"Écran 14 HD - Processeur: Intel Core i3-1115G4 (3.00 GHz up to 4,10 GHz Turbo max , 6 Mo de mémoire cache, Dual-Core) - Système d'exploitation: FreeDos - Mémoire RAM: 4 Go - Disque dur: 1 To HDD - Carte graphique: Intel HD Graphics avec WiFi, Bluetooth, 1x USB 2.0 Type-A, 2x USB 3.2 Gen 1 Type-A, 1x HDMI 1.4,1x RJ-45, 1 prise jack pour casque 3.5 mm - Couleur: Noir - Garantie: 1 an",
    categories:["Gaming","Bureau","Laptop","Laptop Pro"],
    site:"Mytek",
    stock:"rupture de stock",
    price:3600,
  },
]


export function search(priceRange, category) {
  return data.filter(item => {
    const itemPrice = parseInt(item.desc.match(/\d+/)[0]);
    const itemCategory = item.categories.includes(category);
    
    return itemPrice >= priceRange.min && itemPrice <= priceRange.max && itemCategory;
  });
}


