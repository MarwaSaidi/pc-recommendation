import { useState } from "react";
import styled from "styled-components";
import CheckboxSelectOption from "./CheckboxSelectOption";
import { KeyboardArrowDown, KeyboardArrowUp } from "@mui/icons-material";


const CheckboxSelects = styled.div`
  position: relative;
  display: inline-block;
  margin-bottom: ${({ length, isOpen }) => (isOpen ? `${length*40}px` :  "0px")}; 
`;
const SelectContainer = styled.div`
  display: flex;
  position: relative;
`;
const SelectButton = styled.button`
  border: none;
  flex: 1;
  padding: 5px 10px;
  font-size: 16px;
  cursor: pointer;
  color: #f6f6f6;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 21px;
  background-color: #1e1e1e;
  letter-spacing: 0.1px;
`;

const OptionList = styled.ul`
  position: absolute;
  top: 100%;
  left: 0;
  z-index: 1;
  width: 160%;
  background-color: #1e1e1e;
  border-top: none;
  border-radius: 0 0 5px 5px;
  padding: 5px;
  list-style: none;
`;

const Icon = styled.div`
position:absolute;
flex: 1;
left:255px;
`;

const CheckboxSelect = ({ options, selectedValues, onValuesChange, title,length }) => {
  const [isOpen, setIsOpen] = useState(false);

  const handleButtonClick = () => {
    setIsOpen(!isOpen);
  };

  const handleOptionChange = (optionValue) => {
    const isSelected = selectedValues.includes(optionValue);
    const newValues = isSelected
      ? selectedValues.filter((value) => value !== optionValue)
      : [...selectedValues, optionValue];
    onValuesChange(newValues);
  };
  return (
    <CheckboxSelects length={length}isOpen={isOpen }>
      <SelectButton  onClick={handleButtonClick}>
        <SelectContainer>
          {title}
          <Icon>{isOpen ? <KeyboardArrowUp /> : <KeyboardArrowDown />}</Icon>
        </SelectContainer>
      </SelectButton>
      {isOpen && (
        <OptionList >
          {options.map((option) => (
            <CheckboxSelectOption
              key={option.value}
              option={option}
              checked={selectedValues.includes(option.value)}
              onChange={() => handleOptionChange(option.value)}
            />
          ))}
        </OptionList>
      )}
    </CheckboxSelects>
  );
};
export default CheckboxSelect;
