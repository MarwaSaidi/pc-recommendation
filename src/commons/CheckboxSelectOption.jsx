import styled from "styled-components";

const OptionItem = styled.li`
  margin-bottom: 15px;
  height:25px;
  font-size: 16px;
  cursor: pointer;
  &:last-of-type {
    margin-bottom: 0;
  }
`;
const Label = styled.label`
  font-family: "Roboto";
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 24px;
  letter-spacing: 0.4px;
  color: #f6f6f6;
`;

const CheckboxInput = styled.input`
 background-color: #1e1e1e;
 border: 1px solid #ccc;
 border-radius: 2px;
  height: 17px;
  width: 17px;
  margin-right: 15px;
`;
function CheckboxSelectOption({ option, checked, onChange }) {
  return (
    <OptionItem>
      <Label>
        <CheckboxInput
          type="checkbox"
          value={option}
          checked={checked}
          onChange={onChange}
        />
        {option.label}
      </Label>
    </OptionItem>
  );
}

export default CheckboxSelectOption;
