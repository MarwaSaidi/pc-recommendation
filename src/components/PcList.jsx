import { useState, useEffect } from "react";
import { ArrowDropDown, EventBusy, EventAvailable } from "@mui/icons-material";
import { data } from "../data/computerData";
import { useLocation, useNavigate } from "react-router-dom";
import { Button, Categories, CategoriesContainer, Container, Container1, Desc, DescContainer, DescEllipsis, Icon, IconTitle, Image, Info, LaptopList, Title, Title1, Title2, TitleContainer, Website, WebsiteTitle } from "../styles/pcList";
const PcList = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const [type, setType] = useState(location.state.type);
  const [value, setValue] = useState(location.state.value);
  const [items, setItems] = useState([]);
  const handelDetails = () => {
    navigate("/search/details", { state: { value, type } });
  };

  useEffect(() => {
    const filteredItems = data.filter(
      (item) =>
        item.categories.some((category) => category.toLowerCase() === type.toLowerCase()) &&
        item.price >= value[0] &&
        item.price <= value[1]
    );
    setItems(filteredItems);
    setType(location.state.type);
    setValue(location.state.value);
  }, [location.state.type, location.state.value, value, type]);
  return (
    <LaptopList>
      <TitleContainer>
        <Title1>Showing {items.length} Laptops</Title1>
        <Title2>
          Sort by:
          <Icon>
            Best Value <ArrowDropDown />
          </Icon>
        </Title2>
      </TitleContainer>
      {items.length > 0 ? (
        <Container1>
          {items.map((filteredData) => (
            <Container>
              <Info>
                <Image src={filteredData.img} />
                <Title>
                  {filteredData.name}
                  {filteredData.stock === "En Stock" ? (
                    <IconTitle color="3DD598">
                      <EventAvailable />
                    </IconTitle>
                  ) : (
                    <IconTitle color="FC5A5A">
                      <EventBusy />
                    </IconTitle>
                  )}
                </Title>
                <DescContainer>
                  <Desc>{filteredData.desc}</Desc>
                  <DescEllipsis>...</DescEllipsis>
                </DescContainer>
                <CategoriesContainer>
                  {filteredData.categories.map((cat) => (
                    <Categories>{cat}</Categories>
                  ))}
                </CategoriesContainer>
                <Website>
                  <WebsiteTitle>
                    {filteredData.site} . {filteredData.stock}{" "}
                  </WebsiteTitle>
                </Website>
                <Button onClick={handelDetails}>Check it out</Button>
              </Info>
            </Container>
          ))}
        </Container1>
      ) : (
        <div>Loading...</div>
      )}
    </LaptopList>
  );
};

export default PcList;
