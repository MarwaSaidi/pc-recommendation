import {
  Facebook,
  YouTube,
  Telegram,
  Twitter,
  Send,
} from "@mui/icons-material";
import {
  Container,
  Wrapper,
  Left,
  Logo,
  Desc,
  SocialContainer,
  SocialIcon,
  Center,
  Title,
  List,
  ListItem,
  Right,
  Item,
  Div,
  InputContainer,
  Input,
  SendIcon,
} from "../styles/footer";

const Footer = () => {
  return (
    <Container>
      <Wrapper>
        <Left>
          <Logo>PC Recommendation</Logo>
          <Desc>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec
            odio. Praesent libero. Sed cursus ante dapibus diam.
          </Desc>
          <SocialContainer>
            <SocialIcon>
              <Twitter />
            </SocialIcon>
            <SocialIcon>
              <Facebook />
            </SocialIcon>
            <SocialIcon>
              <Telegram />
            </SocialIcon>
            <SocialIcon>
              <YouTube />
            </SocialIcon>
          </SocialContainer>
          <Div>Copyright © 2022 Envast. All rights reserved.</Div>
        </Left>
        <Center>
          <Title>Resoure</Title>
          <List>
            <ListItem>Blogs</ListItem>
            <ListItem>Help and Center</ListItem>
            <ListItem>FaQs</ListItem>
          </List>
        </Center>
        <Right>
          <Title>Subscribe Us</Title>
          <Item>
            Signup for our newsletter to get the latest news in your inbox.
          </Item>
          <Item>
            <InputContainer>
              <Input placeholder="Info@yourgmail.com" />
              <SendIcon color="3749E9">
                <Send />
              </SendIcon>
            </InputContainer>
          </Item>
        </Right>
      </Wrapper>
    </Container>
  );
};

export default Footer;
