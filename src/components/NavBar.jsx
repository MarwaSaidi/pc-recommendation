import React from "react";
import { Navbar, NavContainer, Logo, NavItem,Button } from "../styles/navBar";

const NavBar = () => {
  return (
    <Navbar>
      <NavContainer>
        <Logo> PC Recommendation</Logo>
        <NavItem>
          <Button>Report a problem</Button>
        </NavItem>
      </NavContainer>
    </Navbar>
  );
};

export default NavBar;
