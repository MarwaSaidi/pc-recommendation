import { useState } from "react";
import { LocalAtm, Search } from "@mui/icons-material";
import CheckboxSelect from "../commons/CheckboxSelect";
import { Outlet, useLocation, useNavigate } from "react-router-dom";
import NavBar from "./NavBar";
import Footer from "./Footer";
import {
  Btn,
  Circle1,
  Circle2,
  Circle3,
  Circle4,
  Container,
  Filter,
  Icon,
  Input,
  NLButton,
  NLTitle1,
  NLTitle2,
  NewsLetterContainer,
  NlInput,
  NlInputContainer,
  SearchContainer,
  SearchItem,
  SearchList,
  SidePage,
  Vertical,
  Wrapper,
} from "../styles/pcSearch";
const PcSearch = () => {
  const location = useLocation();
  const [type, setType] = useState(location.state.type);
  const [value, setValue] = useState(location.state.value);
  const [categoriesValues, setCategoriesValues] = useState([""]);
  const [salaryRangeValues, setSalaryRangeValues] = useState([""]);
  const [websiteValues, setWebsiteValues] = useState([""]);
  const [marqueValues, setMarqueValues] = useState([""]);
  const navigate = useNavigate();

  const handleType = (event) => {
    setType(event.target.value);
  };
  const handleValue = (event, index) => {
    const newValue = parseInt(event.target.value);
    if (isNaN(newValue)) {
      return;
    }
    const updatedValue = [...value];
    updatedValue[index] = newValue;
    setValue(updatedValue);
  };
  console.log(type);
  console.log(value[0]);
  console.log(value[1]);
  const Categories = [
    { label: "Gaming", value: "Gaming" },
    { label: " Portable", value: "Portable" },
    { label: "Portable Pro", value: "Portable Pro" },
    { label: "Mac ", value: "Mac" },
    { label: "Bureau", value: "Bureau" },
  ];
  const SalaryRange = [
    { label: "0 TND  -  100 TND", value: "0 TND  -  100 TND" },
    { label: "101 TND  -  200 TND", value: "101 TND  -  200 TND" },
    { label: "201 TND  -  500 TND", value: "201 TND  -  500 TND" },
    { label: "501 TND  -  750 TND", value: "501 TND  -  750 TND" },
    { label: "751 TND  -  1000 TND", value: "751 TND  -  1000 TND" },
    { label: "+1000 TND", value: "+1000 TND" },
  ];
  const website = [];
  const marque = [];
  const handleValuesChange = (newValues) => {
    setCategoriesValues(newValues);
    setSalaryRangeValues(newValues);
    setWebsiteValues(newValues);
    setMarqueValues(newValues);
  };

  const handleSearch = () => {
    navigate("/search/pcList", { state: { value, type } });
  };

  return (
    <>
      <NavBar />
      <Container>
        <Circle1>
          <Circle2>
            <Circle3>
              <Circle4 />
            </Circle3>
          </Circle2>
        </Circle1>

        <SearchContainer>
          <SearchItem>
            <Icon color={"ffd369"}>
              <Search />
            </Icon>
            <Input
              type="text"
              defaultValue={type}
              onChange={handleType}
              content={type}
            />
          </SearchItem>
          <SearchItem>
            <Vertical />
            <Icon color={"ffd369"}>
              <LocalAtm />
            </Icon>
            <Input
              type="number"
              defaultValue={value[0]}
              onChange={(event) => handleValue(event, 0)}
            />{" "}
            <p>TND - </p>
            <Input
              type="number"
              defaultValue={value[1]}
              onChange={(event) => handleValue(event, 1)}
            />
            <p>TND</p>
          </SearchItem>
          <SearchItem>
            <Btn onClick={() => handleSearch(value[0], value[1], type)}>
              Search
            </Btn>
          </SearchItem>
        </SearchContainer>

        <Wrapper>
          <SidePage>
            <NewsLetterContainer>
              <NLTitle1>Join our newsletter</NLTitle1>
              <NLTitle2>Join our newsletter for updated to the system</NLTitle2>
              <NlInputContainer>
                <NlInput type="text" placeholder="Type Your Email" />
                <Icon>
                  <Search />
                </Icon>
              </NlInputContainer>
              <NLButton>Activate</NLButton>
            </NewsLetterContainer>
            <SearchList>
              <CheckboxSelect
                options={Categories}
                selectedValues={categoriesValues}
                onValuesChange={handleValuesChange}
                title={"Categories"}
                length={Categories.length}
              />
            </SearchList>
            <SearchList>
              <CheckboxSelect
                options={SalaryRange}
                selectedValues={salaryRangeValues}
                onValuesChange={handleValuesChange}
                title={"Salary Range"}
                length={SalaryRange.length}
              />
            </SearchList>
            <SearchList>
              <CheckboxSelect
                options={website}
                selectedValues={websiteValues}
                onValuesChange={handleValuesChange}
                title={"Website"}
                length={website.length}
              />
            </SearchList>
            <SearchList>
              <CheckboxSelect
                options={marque}
                selectedValues={marqueValues}
                onValuesChange={handleValuesChange}
                title={"Marque"}
                length={marque.length}
              />
            </SearchList>
          </SidePage>
          <Outlet />
        </Wrapper>
        <Filter />
      </Container>
      <Footer />
    </>
  );
};

export default PcSearch;
