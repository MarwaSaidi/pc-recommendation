import { Share, BookmarkBorder, PanoramaFishEye } from "@mui/icons-material";
import {
  Button,
  ButtonContainer,
  ButtonSeemore,
  Confirm,
  Container,
  DetailsInfo,
  DetailsItem,
  Fiche,
  FicheContent,
  IconBookmark,
  IconContainer,
  IconShare,
  Imag,
  ImgContainer,
  Info,
  Line,
  Name,
  OverView,
  OverViewContent,
  P1,
  P2,
  PcContainer,
  PcImg,
  PcInfo,
  PcInfoSite,
  PcInfoTitle,
  PcPrice,
  PcViewButton,
  PcWrapper,
  PcfavButton,
  Price,
  Properties,
  PropertiesItem,
  PropertiesName,
  PropertiesValue,
  Text,
} from "../styles/pcDetails";
import { Icon } from "@mui/material";

const PcDetails = () => {
  const ficheTeechnique = [
    "Marque:	DELL",
    "Processeur:	Intel Core i3",
    "Mémoire:	4 Go",
    "Fréquence Processeur:	3.00 GHz up to 4,10 GHz Turbo max",
    "Résolution Ecran:	1366 x 768 pixels",
    "Type Processeur	Dual Core",
    "Carte Graphique:	Graphique Intégrée",
    "Garantie	1 an",
  ];
  const images = ["/assets/pc1.png", "/assets/pc2.png", "/assets/pc3.png"];
  const properties = [
    {
      name: "Écran",
      value: "14 Pouces HD",
    },
    {
      name: "Processeur",
      value: "i3-1115G4",
    },
    {
      name: "Mémoire RAM",
      value: "4 Go",
    },
    {
      name: "Disque dur",
      value: "1 To HDD",
    },
  ];

  return (
    <Container>
      <Info>
        <Price>959,000 TND</Price>
        <ImgContainer>
          {images.map((item) => (
            <Imag src={item} />
          ))}
        </ImgContainer>
        <Name>
          Dell Vostro 3400
          <IconContainer>
            <IconBookmark>
              <BookmarkBorder />
            </IconBookmark>
            <IconShare>
              <Share />
            </IconShare>
          </IconContainer>
        </Name>
        <DetailsInfo>
          <DetailsItem>
            {" "}
            <P1>Mytek . </P1> <P2> En Stock</P2>
          </DetailsItem>
          <DetailsItem>
            <P1>PC category . </P1> <P2> Gaming, Ultrabook</P2>
          </DetailsItem>
        </DetailsInfo>
        <Properties>
          {properties.map((item) => (
            <PropertiesItem>
              <PropertiesName>{item.name}</PropertiesName>
              <PropertiesValue>{item.value}</PropertiesValue>
            </PropertiesItem>
          ))}
        </Properties>
        <OverView>Overview</OverView>
        <OverViewContent>
          Écran 14" HD - Processeur: Intel Core i3-1115G4 (3.00 GHz up to 4,10
          GHz Turbo max , 6 Mo de mémoire cache, Dual-Core) - Système
          d'exploitation: FreeDos - Mémoire RAM: 4 Go - Disque dur: 1 To HDD -
          Carte graphique: Intel HD Graphics avec WiFi, Bluetooth, 1x USB 2.0
          Type-A, 2x USB 3.2 Gen 1 Type-A, 1x HDMI 1.4,1x RJ-45, 1 prise jack
          pour casque 3.5 mm - Couleur: Noir - Garantie: 1 an
        </OverViewContent>
        <Fiche>Fiche Technique</Fiche>
        {ficheTeechnique.map((item, index) => (
          <FicheContent>
            <Icon>
              <PanoramaFishEye />
            </Icon>
            {item}
          </FicheContent>
        ))}
        <Line />
        <Confirm>
          <Text>Are you Interested in this Laptop?</Text>
          <Button>Check it out</Button>
        </Confirm>
      </Info>
      <PcContainer>
        <PcWrapper>
          <PcImg src="/assets/laptop.jpg" />
          <PcInfo>
            <PcInfoTitle>Dell Vostro 3400</PcInfoTitle>
            <PcInfoSite>Mytek</PcInfoSite>
            <ButtonContainer>
              <PcViewButton>View</PcViewButton>
              <PcfavButton>
                <BookmarkBorder />
              </PcfavButton>
            </ButtonContainer>
          </PcInfo>
          <PcPrice>959,0 TND</PcPrice>
        </PcWrapper>
        <PcWrapper>
          <PcImg src="/assets/laptop.jpg" />
          <PcInfo>
            <PcInfoTitle>Dell Vostro 3400</PcInfoTitle>
            <PcInfoSite>Mytek</PcInfoSite>
            <ButtonContainer>
              <PcViewButton>View</PcViewButton>
              <PcfavButton>
                <BookmarkBorder />
              </PcfavButton>
            </ButtonContainer>
          </PcInfo>
          <PcPrice>959,0 TND</PcPrice>
        </PcWrapper>
        <ButtonSeemore>SEE MORE</ButtonSeemore>
      </PcContainer>
    </Container>
  );
};

export default PcDetails;
