import { useRef, useEffect, useState } from "react";
import { LocalAtm, ArrowBackIos, ArrowForwardIos } from "@mui/icons-material";
import Slider from "@mui/material/Slider";
import Box from "@mui/material/Box";
import { images } from "../data/computerData";
import { useNavigate } from "react-router-dom";
import {
  Container,
  Circle1,
  Circle2,
  Circle3,
  Circle4,
  Wrapper,
  Title,
  Text,
  Budget,
  TextBudget,
  WrapperSlider,
  Arrow,
  Icon,
  ArrowForward,
  SliderContainer,
  InnerSlider,
  Item,
  Img,
  InfoImg,
  TitleImg,
  Button,
  Div,
  ImgContainer,
  Filter,
  OuterCircle,
  InnerCircle,
  InnerCircle1,
  Image,
} from "../styles/landing";
const Landing = () => {
  const [value, setValue] = useState([500, 2080]);
  const [type, setType] = useState("Gaming");
  const slideRef = useRef();
  const [width, setWidth] = useState(0);
  const navigate = useNavigate();
  const handleMove = (direction) => {
    const slider = slideRef.current;
    const sliderWidth = slider.offsetWidth;
    const currentPosition = slider.scrollLeft;

    const newPosition =
      direction === "l"
        ? currentPosition - sliderWidth
        : currentPosition + sliderWidth;
    slider.scrollTo({
      left: newPosition,
      behavior: "smooth",
    });
  };
  const handleType = (image) => {
    setType(image.title);
  };
  const handleValue = (event, newValue) => {
    setValue(newValue);
  };
  const handelSearch = () => {
    navigate("/search/pcList", { state: { value, type } });
  };

  useEffect(() => {
    setWidth(slideRef.current.scrollWidth - slideRef.current.offsetWidth);
  }, []);
  return (
    <Container>
      <Circle1>
        <Circle2>
          <Circle3>
            <Circle4></Circle4>
          </Circle3>
        </Circle2>
      </Circle1>

      <Wrapper>
        <Title>Making deciscions a bit easier!</Title>
        <Text>What is your budget?</Text>
        <Budget>
          <TextBudget>
            <Icon color="FFD369">
              <LocalAtm />
            </Icon>
            <p>
              {value[0]} TND - {value[1]} TND
            </p>
          </TextBudget>
          <Box sx={{ width: 400, margin: "40px" }}>
            <Slider
              getAriaLabel={() => "Temperature range"}
              value={value}
              onChange={handleValue}
              valueLabelDisplay="auto"
              step={100}
              marks
              min={100}
              max={5000}
              sx={{ color: "#FFD369" }}
            />
          </Box>
        </Budget>
        <Text>Search By Type</Text>
        <WrapperSlider>
          <Arrow>
            <ArrowBackIos onClick={() => handleMove("l")} />
          </Arrow>
          <ArrowForward>
            <ArrowForwardIos onClick={() => handleMove("r")} />
          </ArrowForward>
          <SliderContainer ref={slideRef}>
            <InnerSlider drag="x" dragConstraints={{ right: 0, left: -width }}>
              {images.map((image) => (
                <Item onClick={() => handleType(image)} key={image.id}>
                  <Img src={image.img} />
                  <InfoImg>
                    <TitleImg>{image.title}</TitleImg>
                  </InfoImg>
                </Item>
              ))}
            </InnerSlider>
          </SliderContainer>
        </WrapperSlider>
        <Button onClick={handelSearch}>Search</Button>
      </Wrapper>
      <Div>
        <ImgContainer>
          <Filter />
          <Image src="/assets/man_tshirt.png" />
        </ImgContainer>
        <OuterCircle>
          <InnerCircle>
            <InnerCircle1></InnerCircle1>
          </InnerCircle>
        </OuterCircle>
      </Div>
    </Container>
  );
};

export default Landing;
